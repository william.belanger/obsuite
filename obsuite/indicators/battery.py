#!/usr/bin/python3
import os
from PyQt5 import QtGui, QtCore, QtMultimedia

try:
    from ..backend.common import IndicatorIcon, clamp
except (ImportError, ValueError):
    from backend.common import IndicatorIcon, clamp

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'
INHIBIT_CMD = "systemd-inhibit --what=handle-lid-switch sleep 999d"


class Status(QtCore.QObject):
    popupQueued = QtCore.pyqtSignal(str)
    fetch = QtCore.pyqtSignal()
    play = QtCore.pyqtSignal()
    criticalTimerStart = QtCore.pyqtSignal(int)
    criticalTimerStop = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.preferences = parent.preferences
        self.ready = True
        self.warning = 0
        self.uevent = {}
        self.lastUEvent = {}
        self.uevent = {"current_now": 1, "charge_now": 1, "charge_full": 1, "capacity": 100, "ac": False}
        self.acPath = parent.hasAC()

    def _parse(self):
        self.state = "charging" if self.uevent["ac"] else "discharging"
        self.level = self.uevent["capacity"]
        self.remaining = ""
        if self.uevent["current_now"]:
            if self.uevent["ac"]:
                s = ((self.uevent["charge_full"] - self.uevent["charge_now"]) / self.uevent["current_now"]) * 60 * 60
                if self.level == 100:
                    self.remaining = ""
                else:
                    self.remaining = f"\n{self._timeString(s)} until charged"
            else:
                s = (self.uevent["charge_now"] / self.uevent["current_now"]) * 60 * 60
                self.remaining = f"\n{self._timeString(s)} remaining"

    def _read(self):
        batteries = self.preferences.get("parameters", "battery", "file")
        uevent = {}
        for b in batteries:
            try:
                with open(b) as f:
                    uevent[b] = {}
                    for line in f.readlines():
                        line = line.split("=")
                        propriety = line[0][len("POWER_SUPPLY_"):].lower()
                        try:
                            uevent[b][propriety] = int(line[1].rstrip())  # int
                        except ValueError:
                            pass  # string
            except FileNotFoundError:
                pass
        capacity = 0
        for b in uevent:
            fallbackCurrentNow = uevent[b].get("power_now", 0)
            fallbackChargeNow = uevent[b].get("energy_now", 0)
            fallbackChargeFull = uevent[b].get("energy_full", 0)
            self.uevent["current_now"] += uevent[b].get("current_now", fallbackCurrentNow)
            self.uevent["charge_now"] += uevent[b].get("charge_now", fallbackChargeNow)
            self.uevent["charge_full"] += uevent[b].get("charge_full", fallbackChargeFull)
            capacity += uevent[b].get("capacity", 0)
        self.uevent["capacity"] = int(capacity / len(uevent))

        with open(self.acPath) as f:
            self.uevent["ac"] = bool(int(f.read().rstrip()))

    def _timeString(self, s):
        m, s = divmod(s, 60)
        h, m = divmod(m, 60)
        return "%02d:%02d:%02d" % (h, m, s)

    def update(self):
        criticalThreshold = self.preferences.get("parameters", "battery", "critical threshold")
        lowThreshold = self.preferences.get("parameters", "battery", "low threshold")

        self._read()
        self._parse()
        if not self.lastUEvent:
            self._read()
            self.lastUEvent = dict(self.uevent)

        if self.level == 100:
            self.icon = "full"
        if self.level < criticalThreshold and self.state == "charging":
            self.icon = "charging_1"
        elif self.level < criticalThreshold and self.state == "discharging":
            self.icon = "critical"
        else:
            suffix = round((self.level * 7) / (100 - criticalThreshold))
            suffix = clamp(suffix, 1, 7)
            self.icon = f"{self.state}_{suffix}"

        if self.uevent["ac"]:
            self.warning = 0
            self.criticalTimerStop.emit()
            if not self.lastUEvent["ac"]:
                self.popupQueued.emit(f"AC plugged in\n{self.level}%")

        elif self.warning < 2 and self.level < criticalThreshold:
            delay = self.preferences.get("parameters", "battery", "critical delay")
            self.play.emit()
            self.warning = 2
            self.criticalTimerStart.emit(delay * 60 * 1000)
            self.popupQueued.emit(f"Critical battery level\n{self.level}%")

        elif self.warning < 1 and self.level < lowThreshold:
            self.warning = 1
            self.popupQueued.emit(f"Low battery\n{self.level}%")

        elif self.lastUEvent["ac"]:
            self.popupQueued.emit(f"AC unplugged\n{self.level}%")

        if self.level > criticalThreshold:
            self.criticalTimerStop.emit()
        self.lastUEvent = dict(self.uevent)
        self.fetch.emit()


class Main(IndicatorIcon):
    def _initHook(self):
        self.criticalTimer = QtCore.QTimer(singleShot=True)
        self.criticalTimer.timeout.connect(self._critical)
        self.status = Status(self)
        self.status.criticalTimerStop.connect(self.criticalTimer.stop)
        self.status.criticalTimerStart.connect(self.criticalTimer.start)
        self.status.play.connect(self._play)
        self.status.popupQueued.connect(self.popupQueued.emit)
        self.systemdInhibit = QtCore.QProcess()
        self.alert = QtMultimedia.QSound(f"{LOCAL_DIR}../ui/sounds/critical.wav", self)

        self.iconsInhibit = {}
        for icon in os.listdir(f"{LOCAL_DIR}../ui/icons/battery/inhibit"):
            basename = os.path.splitext(icon)[0]
            path = f"{LOCAL_DIR}../ui/icons/battery/inhibit/{icon}"
            self.iconsInhibit[basename] = QtGui.QIcon(path)

        self.parent.initIndicator(self, "battery")

    def _critical(self):
        if self.preferences.get("parameters", "battery", "critical"):
            cmd = self.preferences.get("parameters", "battery", "critical command")
            self.log.warning(f"Execution of '{cmd}'")
            self.slave.start(cmd)
            self.slave.waitForFinished()
            self.fetchTimer.start()  # Reset timer
        else:
            self.log.warning("Critical action ignored")
            self._play()
            delay = self.preferences.get("parameters", "battery", "critical delay") * 60 * 1000
            self.criticalTimer.start(delay)  # Reset timer
            self.popupQueued.emit(f"Battery level ({self.status.level}%)")

    def _fetch(self):
        if self._hasBattery() and self.hasAC():
            self.status.update()

            if self.systemdInhibit.state() == QtCore.QProcess.Running:
                icon = self.iconsInhibit[self.status.icon]
                systemdInhibit = "\nLid switch inhibited"
            else:
                icon = self.icons[self.status.icon]
                systemdInhibit = ""
            self.setIcon(icon)
            self.setToolTip(f"Battery level: {self.status.level}%{self.status.remaining}{systemdInhibit}")
        else:
            # Device not found
            options = os.listdir("/sys/class/power_supply")
            self.log.error(f"Device(s) not found. Available: {options}")
            self.error.emit(QtCore.QProcess.FailedToStart)

    def _hasBattery(self):
        for f in self.preferences.get("parameters", "battery", "file"):
            if os.path.isfile(f):
                return True
        return False

    def _play(self):
        if self.preferences.get("parameters", "battery", "sound"):
            self.alert.play()

    def action(self, action, command=""):
        if action == "exec":
            self.execute.emit(command)

        elif action == "toggle systemd inhibit":
            if self.systemdInhibit.state() == QtCore.QProcess.Running:
                self.systemdInhibit.terminate()
                self.popupQueued.emit("Systemd inhibit stopped")
            else:
                self.systemdInhibit.start(INHIBIT_CMD)
                self.popupQueued.emit("Systemd inhibit started")

        elif action == "reset critical timer":
            if self.criticalTimer.isActive():
                delay = self.preferences.get("parameters", "battery", "critical delay") * 60 * 1000
                self.criticalTimer.start(delay)  # Reset timer
                self.popupQueued.emit("Critical timer has been reset")

        else:
            self.log.error(f"Invalid action '{action}'")

    def hasAC(self):
        for f in os.listdir("/sys/class/power_supply"):
            if f.startswith("AC") or f.startswith("ADP"):
                path = f"/sys/class/power_supply/{f}/online"
                if os.path.isfile(path):
                    return path
        return None
